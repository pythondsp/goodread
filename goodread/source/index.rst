.. Goodread documentation master file, created by
   sphinx-quickstart on Mon Aug 21 10:19:58 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GoodRead
========


.. only:: html

    :download:`Click here <goodread/kindle/Prayers - Meher Krishna Patel.azw3>` to download the Prayers in Kindle-format.



.. toctree::
   :maxdepth: 1
   :caption: Contents:
   
   goodread/prayer
   goodread/teachings
   goodread/quotes
   goodread/proverbs
