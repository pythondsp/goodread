Prayers
*******


.. only:: html

    :download:`Click here <kindle/Prayers - Meher Krishna Patel.azw3>` to download the Prayers in Kindle-format.

.. raw:: html 

    <br><br>


Parvardigar Prayer
==================

**Universal Prayer (Master's Prayer) given by Meher Baba on 13 September 1953**

.. raw:: html

    O Parvardigar! The Preserver and Protector of All,<br>
    You are without beginning and without end.<br>
    <br>
    Non-dual, beyond comparison,<br>
    and none can measure You.<br>
    <br>
    You are without color, without expression,<br>
    without form and without attributes.<br>
    <br>
    You are unlimited and unfathomable; <br>
    beyond imagination and conception;<br>
    eternal and imperishable.<br>
    <br>
    You are indivisible;<br>
    and none can see you but with eyes divine.<br>
    <br>
    You always were, You always are,<br>
    and You always will be.<br>
    <br>
    You are everywhere, You are in everything, and<br>
    You are also beyond everywhere and beyond everything.<br>
    <br>
    You are in the firmament and in the depths,<br>
    You are manifest and unmanifest;<br>
    on all planes and beyond all planes.<br>
    <br>
    You are in the three worlds,<br>
    and also beyond the three worlds.<br>
    <br>
    You are imperceptible and independent.<br>
    You are the Creator, the Lord of Lords,<br>
    the Knower of all minds and hearts.<br>
    <br>
    You are Omnipotent and Omnipresent.<br>
    You are Knowledge Infinite, Power Infinite and Bliss Infinite.<br>
    <br>
    You are the Ocean of Knowledge,<br>
    All-knowing, Infinitely-knowing;<br>
    the Knower of the past, the present and the future;<br>
    and You are Knowledge itself.<br>
    <br>
    You are all-merciful and eternally benevolent.<br>
    You are the Soul of souls, the One with infinite attributes.<br>
    <br>
    You are the Trinity of Truth, Knowledge and Bliss;<br>
    You are the Source of Truth, the Ocean of Love.<br>
    <br>
    You are the Ancient One, the Highest of the High.<br>
    You are Prabhu and Parameshwar;<br>
    You are the Beyond God and the Beyond-Beyond God also;<br>
    You are Parabrahma, Allah, Elahi, Yezdan,<br>
    Ahuramazda, and God the Beloved.<br>
    <br>
    You are named Ezad, the Only One Worthy of Worship. <br>
    <br><br><br>

.. raw:: latex
    
    
    \vspace{4mm}
    O Parvardigar! The Preserver and Protector of All,
    
    You are without beginning and without end.
    
    
    
    Non-dual, beyond comparison,
    
    and none can measure You.
    
    
    
    You are without color, without expression,
    
    without form and without attributes.
    
    
    
    You are unlimited and unfathomable; 
    
    beyond imagination and conception;
    
    eternal and imperishable.
    
    
    
    You are indivisible;
    
    and none can see you but with eyes divine.
    
    
    
    You always were, You always are,
    
    and You always will be.
    
    
    
    You are everywhere, You are in everything, and
    
    You are also beyond everywhere and beyond everything.
    
    
    
    You are in the firmament and in the depths,
    
    You are manifest and unmanifest;
    
    on all planes and beyond all planes.
    
    
    
    You are in the three worlds,
    
    and also beyond the three worlds.
    
    
    
    You are imperceptible and independent.
    
    You are the Creator, the Lord of Lords,
    
    the Knower of all minds and hearts.
    
    
    
    You are Omnipotent and Omnipresent.
    
    You are Knowledge Infinite, Power Infinite and Bliss Infinite.
    
    
    
    You are the Ocean of Knowledge,
    
    All-knowing, Infinitely-knowing;
    
    the Knower of the past, the present and the future;
    
    and You are Knowledge itself.
    
    
    
    You are all-merciful and eternally benevolent.
    
    You are the Soul of souls, the One with infinite attributes.
    
    
    
    You are the Trinity of Truth, Knowledge and Bliss;
    
    You are the Source of Truth, the Ocean of Love.
    
    
    
    You are the Ancient One, the Highest of the High.
    
    You are Prabhu and Parameshwar;
    
    You are the Beyond God and the Beyond-Beyond God also;
    
    You are Parabrahma, Allah, Elahi, Yezdan,
    
    Ahuramazda, and God the Beloved.
    
    
    
    You are named Ezad, the Only One Worthy of Worship. 
    
    


Prayer of Repentance
====================

**Given by Meher Baba on 8 November 1952**

.. raw:: html

    We repent, O God most merciful; for all our sins;<br>
    for every thought that was false or unjust or unclean;<br>
    for every word spoken that ought not to have been spoken;<br>
    for every deed done that ought not to have been done.<br><br>

    We repent for every deed and word and thought inspired by selfishness,<br>
    and for every deed and word and thought inspired by hatred.<br><br>

    We repent most specially for every lustful thought and every lustful action;<br>
    for every lie; for all hypocrisy;<br>
    for every promise given but not fulfilled,<br>
    and for all slander and back-biting.<br><br>

    Most specially also, we repent for every action that has brought ruin to others;<br>
    for every word and deed that has given others pain;<br>
    and for every wish that pain should befall others.<br><br>

    In your unbounded mercy, we ask you to forgive us, O God,<br>
    for all these sins committed by us,<br>
    and to forgive us for our constant failures<br>
    to think and speak and act according to your will.<br>
    <br><br><br>

.. raw:: latex

    \vspace{4mm}
    We repent, O God most merciful; for all our sins;

    for every thought that was false or unjust or unclean;

    for every word spoken that ought not to have been spoken;

    for every deed done that ought not to have been done.

    \vspace{3mm}
    We repent for every deed and word and thought inspired by selfishness,

    and for every deed and word and thought inspired by hatred.

    \vspace{3mm}
    We repent most specially for every lustful thought and every lustful action;

    for every lie; for all hypocrisy;

    for every promise given but not fulfilled,

    and for all slander and back-biting.

    \vspace{3mm}
    Most specially also, we repent for every action that has brought ruin to others;

    for every word and deed that has given others pain;

    and for every wish that pain should befall others.

    \vspace{3mm}
    In your unbounded mercy, we ask you to forgive us, O God,

    for all these sins committed by us,

    and to forgive us for our constant failures

    to think and speak and act according to your will.


Beloved God Prayer
==================

.. raw:: html

    Beloved God, help us all to love You more and more, <br>
    and more and more and still yet more, <br>
    till we become worthy of union with You; <br>
    and help us all to hold fast to Baba's daaman till the very end. 
    <br><br><br>


.. raw:: latex

    \vspace{4mm}
    Beloved God, help us all to love You more and more, 

    and more and more and still yet more, 

    till we become worthy of union with You; 

    and help us all to hold fast to Baba's daaman till the very end. 
    

The Australian Aarti
====================

**By Francis Brabazon** 

.. raw:: html

    O glorious, eternal Ancient One <br>
    Your face is a bright, transcendental Sun<br>
    Lighten this dark world and the tears I weep;<br>
    My heart, Meher, I give to you to keep.<br><br>

    Creator, yet creationless you are<br>
    Truth and Truth's Body, divine Avatar<br>
    Who, through compassion the three worlds maintains<br>
    Destroy this ignorance that life sustains.<br><br>

    These five lights are the whirling spokes of breath<br>
    Of the worlds-wheel that bears me on to death<br>
    Unless you, who are infinitely kind<br>
    Break the wheel's hub which is conditioned mind.<br><br>

    This incense is my love, these fruits my art<br>
    Which to please you I have shaped from my heart<br>
    Accept them as you would a simple flower<br>
    That has no use beyond its shining hour.<br><br>

    You are my Self, I sing to you in praise<br>
    And beg your love to bear me through the days<br>
    Till you, the Everliving Perfect One<br>
    Illume my darkness with your shining Sun.<br><br>
    <br>


.. raw:: latex

    \vspace{4mm}
    O glorious, eternal Ancient One 

    Your face is a bright, transcendental Sun

    Lighten this dark world and the tears I weep;

    My heart, Meher, I give to you to keep.

    \vspace{4mm}
    Creator, yet creationless you are

    Truth and Truth's Body, divine Avatar

    Who, through compassion the three worlds maintains

    Destroy this ignorance that life sustains.


    \vspace{4mm}
    These five lights are the whirling spokes of breath

    Of the worlds-wheel that bears me on to death

    Unless you, who are infinitely kind

    Break the wheel's hub which is conditioned mind.


    \vspace{4mm}
    This incense is my love, these fruits my art

    Which to please you I have shaped from my heart

    Accept them as you would a simple flower

    That has no use beyond its shining hour.


    \vspace{4mm}
    You are my Self, I sing to you in praise

    And beg your love to bear me through the days

    Till you, the Everliving Perfect One

    Illume my darkness with your shining Sun.

    


The American Aarti
==================

**By Hank Mindlin**

.. raw:: html

    How can one fathom your Fathomless being?<br>
    How can we know you we see with gross eyes?<br>
    A glimpse of your shadow has blinded ouir seeing;<br>
    How can your Glory ere be realized.<br>
    <br>

    Consumed is my mind in your fire and flame;<br>
    Accept it O Meher, in Oneness.<br>
    Consumed is my heart in the sound your name.<br>
    Accept, O Meher, my Arti;<br>
    Accept, O Meher, my song.<br>
    <br>

    Thoughts cannot reach you and words cannot speak you,<br>
    Infinite ocean of unending bliss.<br>
    Though we beseech you you, how can we seek you?<br>
    How can the finite know limitlessness?<br>
    <br>

    Consumed is my mind in your fire and flame;<br>
    Accept it O Meher, in Oneness.<br>
    Consumed is my heart in the sound your name.<br>
    Accept, O Meher, my Arti;<br>
    Accept, O Meher, my song.<br>
    <br>

    At your command, suns and stars give their light;<br>
    What in the worlds can I offer as mine?<br>
    Even my gift of love would be naught in your sight,<br>
    But veiled reflections of your Love Divine.<br>
    <br>

    Consumed is my mind in your fire and flame;<br>
    Accept it O Meher, in Oneness.<br>
    Consumed is my heart in the sound your name.<br>
    Accept, O Meher, my Arti;<br>
    Accept, O Meher, my song.<br>
    <br>

    You are the Ancient One, Lord of Creation,<br>
    How can we measure your true majesty?<br>
    You are the Christ, the Divine Incarnation,<br>
    Dear Lord, please don't be indifferent to me.<br>
    <br>

    Consumed is my mind in your fire and flame;<br>
    Accept it O Meher, in Oneness.<br>
    Consumed is my heart in the sound your name.<br>
    Accept, O Meher, my Arti;<br>
    Accept, O Meher, my song.<br>
    <br>

    You are the beginning and end of all things;<br>
    ‘Tis you alone who assumes every role.<br>
    Sinners and saints, beggars and kings,<br>
    You are the Source and you are the Goal.<br>
    <br>

    Consumed is my mind in your fire and flame;<br>
    Accept it O Meher, in Oneness.<br>
    Consumed is my heart in the sound your name.<br>
    Accept, O Meher, my Arti;<br>
    Accept, O Meher, my song.<br>
    <br>

    How can one fathom your Fathomless being?<br>
    How can we know you we see with gross eyes?<br>
    A glimpse of your shadow has blinded our seeing;<br>
    How can your Glory ere be realized.<br>
    <br>

    Consumed is my mind in your fire and flame;<br>
    Accept it O Meher, in Oneness.<br>
    Consumed is my heart in the sound your name.<br>
    Accept, O Meher, my Arti;<br>
    Accept, O Meher, my song.   <br>
    <br>


.. raw:: latex

    
    \vspace{4mm}    
    How can one fathom your Fathomless being?
    
    How can we know you we see with gross eyes?
    
    A glimpse of your shadow has blinded ouir seeing;
    
    How can your Glory ere be realized.
    
    
    
    \vspace{4mm}
    Consumed is my mind in your fire and flame;
    
    Accept it O Meher, in Oneness.
    
    Consumed is my heart in the sound your name.
    
    Accept, O Meher, my Arti;
    
    Accept, O Meher, my song.
    
    
    
    \vspace{4mm}
    Thoughts cannot reach you and words cannot speak you,
    
    Infinite ocean of unending bliss.
    
    Though we beseech you you, how can we seek you?
    
    How can the finite know limitlessness?
    
    
    
    \vspace{4mm}
    Consumed is my mind in your fire and flame;
    
    Accept it O Meher, in Oneness.
    
    Consumed is my heart in the sound your name.
    
    Accept, O Meher, my Arti;
    
    Accept, O Meher, my song.
    
    
    
    \vspace{4mm}
    At your command, suns and stars give their light;
    
    What in the worlds can I offer as mine?
    
    Even my gift of love would be naught in your sight,
    
    But veiled reflections of your Love Divine.
    
    
    
    \vspace{4mm}
    Consumed is my mind in your fire and flame;
    
    Accept it O Meher, in Oneness.
    
    Consumed is my heart in the sound your name.
    
    Accept, O Meher, my Arti;
    
    Accept, O Meher, my song.
    
    
    
    \vspace{4mm}
    You are the Ancient One, Lord of Creation,
    
    How can we measure your true majesty?
    
    You are the Christ, the Divine Incarnation,
    
    Dear Lord, please don't be indifferent to me.
    
    
    
    \vspace{4mm}
    Consumed is my mind in your fire and flame;
    
    Accept it O Meher, in Oneness.
    
    Consumed is my heart in the sound your name.
    
    Accept, O Meher, my Arti;
    
    Accept, O Meher, my song.
    
    
    
    \vspace{4mm}
    You are the beginning and end of all things;
    
    ‘Tis you alone who assumes every role.
    
    Sinners and saints, beggars and kings,
    
    You are the Source and you are the Goal.
    
    
    
    \vspace{4mm}
    Consumed is my mind in your fire and flame;
    
    Accept it O Meher, in Oneness.
    
    Consumed is my heart in the sound your name.
    
    Accept, O Meher, my Arti;
    
    Accept, O Meher, my song.
    
    
    
    \vspace{4mm}
    How can one fathom your Fathomless being?
    
    How can we know you we see with gross eyes?
    
    A glimpse of your shadow has blinded our seeing;
    
    How can your Glory ere be realized.
    
    
    
    \vspace{4mm}
    Consumed is my mind in your fire and flame;
    
    Accept it O Meher, in Oneness.
    
    Consumed is my heart in the sound your name.
    
    Accept, O Meher, my Arti;
    
    Accept, O Meher, my song.   
    
    
    